// Объявляем различные виды переменных, с помощью директивы let. 
let str = "Приветик, меня зовут Алина!" //string - строка, то есть последовательность символов.
let num = 14566514684365; // number - число
let bool = true; //bool - логическое значение, правда
let arr = [15, 17, 8, 64, 35]; // array - массив
let obj = {key: "value"}; // объект


// Даллее выведем типы данных
console.log(typeof num); // number
console.log(typeof str); // string
console.log(typeof bool); // boolean
console.log(typeof bool); // boolean
console.log(typeof arr); // object (в выводе учитывает и массивы и объекты)
console.log(typeof obj); // object


// Максимальные и минимальные значения числовых типов данных,существуют числа с плавающей точкой (float)
let maxInt = Number.MAX_SAFE_INTEGER;//это максимальное безопасное целое число
let minInt = Number.MIN_SAFE_INTEGER; //это минимальное безопасное целое число
let maxFloat = Number.MAX_VALUE;//это максимальное положительное число с плавающей точкой
let minFloat = Number.MIN_VALUE;// это минимальное положительное число с плавающей точкой
// Примеры различных значений
console.log(maxInt); // 599565658
console.log(minInt); // -45645645654
console.log(maxFloat); // 54.5858445+554
console.log(minFloat); // 8768e-456564566
